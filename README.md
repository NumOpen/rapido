# Create your website with Rapido. Edit, publish and share collaborative content.

![Screenshot of a page edited with Rapido](docs/Rapido screenshot.png)

# Features

- Create webpages and edit them with [Quill](https://quilljs.com) WYSIWYG editor;
- Duplicate a page;
- Add a menu with links to local pages or remote URLs. Menu items can be reordered by drag and drop;
- Save a page at different times, view the page history and revert to a previous version;
- Share a page by email;
- Invite other people to become coauthors of your pages;
- With the file manager, add an image or a video to multiple pages;
- Insert contact forms in pages to allow people to contact you.

# Highlights

- Rapido is very fast;
- Rapido is very easy to use;
- Rapido does not use any external resources such as APIs, fonts and other tracking tools.

<a href="https://framagit.org/InfoLibre/rapido/-/blob/master/CONTRIBUTING.md"><img src="docs/Install and run.png" alt="Install and run"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="https://framagit.org/InfoLibre/rapido/-/raw/master/docs/Users'%20and%20developers'%20manual.odt"><img src="docs/Users' and developers' manual.png" alt="Users' and developers' manual"></a>

# Contribute

To report a bug, translate Rapido or improve code and documentation, see [CONTRIBUTING.md](CONTRIBUTING.md).

<a href="https://diaspora-fr.org/u/rapido"><img src="docs/diaspora*.png" alt="Share on diaspora*"></a>&nbsp;&nbsp;&nbsp;&nbsp;<a rel="me" href="https://fosstodon.org/@Rapido"><img src="docs/Mastodon.png" alt="Follow us on Mastodon"></a>

# Authors and license

[David VANTYGHEM](mailto:david.vantyghem@laposte.net), [Azzam A.I](mailto:azzamai91@gmail.com), [Widiyaksa A](mailto:widiyaksa@gmail.com), [Louis LAUGIER](mailto:l.laugier@protonmail.com), [F PORTIER](mailto:f.portierdev@protonmail.com), Jacky FRANCOIS (english translation).\
**Rapido** is a complete rewrite of razorCMS from [Paul SMITH](https://github.com/smiffy6969).

**Rapido** is [Free and Open Source Software](https://gnu.org/philosophy/free-sw.html). You can redistribute it and/or modify it under the terms of the [GNU Affero General Public License](COPYING).
